module.exports = {
    publicPath: './',
    productionSourceMap: false,
    devServer: {
        // port: "8081", //代理端口
        // open: true, //项目启动时是否自动打开浏览器，我这里设置为false,不打开，true表示打开
        proxy: {
            '/': { //代理api
                target: "http://bydlapi.jinjifuweng.com/", //服务器api地址
                changeOrigin: true, //是否跨域
                ws: false, // proxy websockets
                pathRewrite: { //重写路径
                    "^/": ''
                }
            }
        }
    }
}